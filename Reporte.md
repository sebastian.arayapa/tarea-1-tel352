---
title: "Tarea 1: Algoritmos de Búsqueda en Pacman"
author: "Gabriela Bustamante"
date: "12/10/2021"
---
# Tarea 1: Algoritmos de Búsqueda en Pacman

link video explicativo: 

En el siguiente documento expondre las modificacion efectuadas al problema original y la comparacion de los resultados respecto al original.

En esta ocasion elegí cambiar el Ambiente. Para esto primero se debe analizar las propiedades del problema, saber cuales son las condiciones a las que nos enfretamos
- ambiente parcial o completamente observable
- uno o multiples agentes
- determinista o estocastico
- secuenciao o episodico
- estatico o dinamico
- descreto o continuo

En esta ocasion elegí cambiar los algoritmos propuestos. Para eso implementé otro algoritmo que no se encontraba en los problemas implementados, 
