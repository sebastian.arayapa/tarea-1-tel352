# Tarea 1 TEL352

En este repositorio encontrará la carpeta _Codigos_, que corresponde a las soluciones para los problemas del 1 al 4 propuestos en la pagina:

`https://inst.eecs.berkeley.edu/~cs188/sp21/project1/`

Tambien encontrará un archivo _Reporte.md_ donde explico mis modificación y resultados.

A continuacion dejare los comandos para revisar la ejecución de cada ejercicio.

### Ejercicio 1 :  Finding a Fixed Food Dot using Depth First Search
~~~
python pacman.py -l tinyMaze -p SearchAgent 
python pacman.py -l mediumMaze -p SearchAgent
python pacman.py -l bigMaze -z .5 -p SearchAgent
~~~

### Ejercicio 2 :  Breadth First Search
~~~
python pacman.py -l mediumMaze -p SearchAgent -a fn=bfs
python pacman.py -l bigMaze -p SearchAgent -a fn=bfs -z .5
python eightpuzzle.py
~~~

### Ejercicio 3 :  A* search
~~~
python pacman.py -l bigMaze -z .5 -p SearchAgent -a fn=astar,heuristic=manhattanHeuristic
~~~

### Ejercicio 4 :  Finding All the Corners
~~~
python pacman.py -l tinyCorners -p SearchAgent -a fn=bfs,prob=CornersProblem
python pacman.py -l mediumCorners -p SearchAgent -a fn=bfs,prob=CornersProblem
~~~
